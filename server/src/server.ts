const express = require('express');
import cors from 'cors';
const app = express();
import routes from './api/api';

app.use(express.static('public'));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

routes(app);

const startServer = async () => {
  try {
    await app.listen(3001);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
};

startServer();
