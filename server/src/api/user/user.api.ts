import { Router } from 'express';
import {
  readUsers,
  getUserById,
  deleteUser,
  postUser,
  updateUser,
  getUserByLogin,
  getUserByName,
  login,
} from '../../services/user/user.service';
import {
  deleteMessageByUser,
  updateMessageByUser,
} from '../../services/message/message.service';
import {
  NOT_FOUND_USER,
  LOGIN_ERROR,
  UPDATE_LOGIN_ERROR,
  UPDATE_NAME_ERROR,
  ADD_LOGIN_ERROR,
  ADD_NAME_ERROR,
} from '../../constants/errors';

const router = Router();

router.get('/', (_, res) => {
  res.send(readUsers());
});

router.post('/', (req, res) => {
  const userByLogin = getUserByLogin(req.body.login);
  if (userByLogin && userByLogin.id !== req.body.id)
    return res.status(400).send(ADD_LOGIN_ERROR);
  const userByName = getUserByName(req.body.name);
  if (userByName && userByName.id !== req.body.id)
    return res.status(400).send(ADD_NAME_ERROR);
  res.send(postUser(req.body));
});

router.post('/login', (req, res) => {
  const user = login(req.body);
  if (!user) {
    res.status(401).send(LOGIN_ERROR);
  }
  res.send(user);
});

router.put('/:id', (req, res) => {
  const userByLogin = getUserByLogin(req.body.login);
  if (userByLogin && userByLogin.id !== req.body.id)
    return res.status(400).send(UPDATE_LOGIN_ERROR);
  const userByName = getUserByName(req.body.name);
  if (userByName && userByName.id !== req.body.id)
    return res.status(400).send(UPDATE_NAME_ERROR);
  const user = getUserById(req.params.id);
  if (!user) res.status(404).send(NOT_FOUND_USER);
  const users = updateUser(req.body);
  updateMessageByUser(req.body);
  res.send(users);
});

router.get('/:id', (req, res) => {
  const user = getUserById(req.params.id);
  if (!user) res.status(404).send(NOT_FOUND_USER);
  res.send(user);
});

router.delete('/:id', (req, res) => {
  const user = getUserById(req.params.id);
  if (!user) res.status(404).send(NOT_FOUND_USER);
  const users = deleteUser(req.params.id);
  deleteMessageByUser(req.params.id);
  res.send(users);
});

export default router;
