import { Router } from 'express';
import {
  readMessages,
  getMessageById,
  deleteMessage,
  postMessage,
  updateMessage,
} from '../../services/message/message.service';
import { NOT_FOUND_MESSAGE } from '../../constants/errors';

const router = Router();

router.get('/', (_, res) => {
  res.send(readMessages());
});

router.post('/', (req, res) => {
  res.send(postMessage(req.body));
});

router.put('/:id', (req, res) => {
  const message = getMessageById(req.params.id);
  if (!message) res.status(404).send(NOT_FOUND_MESSAGE);
  res.send(updateMessage(req.body));
});

router.get('/:id', (req, res) => {
  const message = getMessageById(req.params.id);
  if (!message) res.status(404).send(NOT_FOUND_MESSAGE);
  res.send(message);
});

router.delete('/:id', (req, res) => {
  const message = getMessageById(req.params.id);
  if (!message) res.status(404).send(NOT_FOUND_MESSAGE);
  const messages = deleteMessage(req.params.id);
  res.send(messages);
});

export default router;
