import messageRoutes from './message/message.api';
import userRoutes from './user/user.api';
import { Express } from 'express';
export default (app: Express) => {
  app.use('/messages', messageRoutes);
  app.use('/users', userRoutes);
};
