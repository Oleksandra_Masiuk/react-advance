const NOT_FOUND_USER = 'Cannot find user';
const NOT_FOUND_MESSAGE = 'Cannot find message';
const LOGIN_ERROR = 'Cannot login';
const UPDATE_LOGIN_ERROR = 'Cannot set user this login';
const UPDATE_NAME_ERROR = 'Cannot set user this name';
const ADD_LOGIN_ERROR = 'Cannot add user with this login';
const ADD_NAME_ERROR = 'Cannot add user with this name';

export {
  NOT_FOUND_USER,
  LOGIN_ERROR,
  UPDATE_LOGIN_ERROR,
  UPDATE_NAME_ERROR,
  ADD_LOGIN_ERROR,
  ADD_NAME_ERROR,
  NOT_FOUND_MESSAGE,
};
