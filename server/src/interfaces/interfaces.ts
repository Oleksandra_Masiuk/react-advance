interface IMessage {
  avatar: string | null;
  createdAt: string;
  editedAt: string | null;
  id: string | null | undefined;
  text: string;
  user: string;
  userId: string;
  isLiked: Array<string>;
}

interface IUser {
  role: number;
  avatar: string | null;
  id: string | null | undefined;
  login: string;
  name: string;
  password: string;
}

interface ICredentials {
  login: string;
  password: string;
}

export { IMessage, IUser, ICredentials };
