import fs from 'fs';
import path from 'path';
import { v4 as uuid } from 'uuid';

import { IMessage, IUser } from '../../interfaces/interfaces';

const dataPath = `${path.resolve()}/src/data/messages.json`;

const readMessages = () => {
  const text = fs.readFileSync(dataPath, 'utf-8');
  const all = JSON.parse(text);
  return all;
};

const getMessageById = (id: string) => {
  const items = readMessages();
  return items.find((message: IMessage) => message.id === id);
};

const deleteMessage = (id: string) => {
  const messages = readMessages();
  const updatedMessages = messages.filter(
    (message: IMessage) => message.id !== id
  );
  const jsonText = JSON.stringify(updatedMessages, null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return updatedMessages;
};

const deleteMessageByUser = (id: string) => {
  const messages = readMessages();
  const updatedMessages = messages.filter(
    (message: IMessage) => message.userId !== id
  );
  const jsonText = JSON.stringify(updatedMessages, null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return updatedMessages;
};

const postMessage = (message: IMessage) => {
  const messages = readMessages();
  const id = uuid();
  const jsonText = JSON.stringify(
    [...messages, { ...message, id, createdAt: new Date() }],
    null,
    2
  );
  fs.writeFileSync(dataPath, jsonText);
  return getMessageById(id);
};

const updateMessage = (message: IMessage) => {
  const messages = readMessages();
  const updatedMessages = messages.map((mes: IMessage) =>
    mes.id !== message.id ? mes : { ...message }
  );
  const jsonText = JSON.stringify([...updatedMessages], null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return readMessages();
};

const updateMessageByUser = (us: IUser) => {
  const messages = readMessages();
  const updatedMessages = messages.map((mes: IMessage) =>
    mes.userId !== us.id ? mes : { ...mes, user: us.name }
  );
  const jsonText = JSON.stringify([...updatedMessages], null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return readMessages();
};

export {
  readMessages,
  getMessageById,
  deleteMessage,
  postMessage,
  updateMessage,
  deleteMessageByUser,
  updateMessageByUser,
};
