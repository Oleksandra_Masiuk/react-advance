import fs from 'fs';
import path from 'path';
import { v4 as uuid } from 'uuid';

import { IUser, ICredentials } from '../../interfaces/interfaces';

const dataPath = `${path.resolve()}/src/data/users.json`;

const readUsers = () => {
  const text = fs.readFileSync(dataPath, 'utf-8');
  const all = JSON.parse(text);
  return all;
};

const getUserById = (id: string) => {
  const items = readUsers();
  return items.find((user: IUser) => user.id === id);
};

const getUserByLogin = (login: string) => {
  const items = readUsers();
  return items.find((user: IUser) => user.login === login);
};

const getUserByName = (name: string) => {
  const items = readUsers();
  return items.find((user: IUser) => user.name === name);
};

const deleteUser = (id: string) => {
  const users = readUsers();
  const updatedUsers = users.filter((user: IUser) => user.id !== id);
  const jsonText = JSON.stringify(updatedUsers, null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return updatedUsers;
};

const postUser = (user: IUser) => {
  const users = readUsers();
  const id = uuid();
  const jsonText = JSON.stringify([...users, { ...user, id }], null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return getUserById(id);
};

const updateUser = (user: IUser) => {
  const users = readUsers();
  const updatedUsers = users.filter((us: IUser) => us.id !== user.id);
  const jsonText = JSON.stringify([...updatedUsers, user], null, 2);
  fs.writeFileSync(dataPath, jsonText);
  return readUsers();
};

const login = (credentials: ICredentials) => {
  const users = readUsers();
  return users.find(
    (us: IUser) =>
      us.login === credentials.login && us.password === credentials.password
  );
};

export {
  readUsers,
  getUserById,
  deleteUser,
  postUser,
  updateUser,
  login,
  getUserByName,
  getUserByLogin,
};
