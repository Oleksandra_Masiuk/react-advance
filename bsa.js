import Chat from './client/src/components/Chat';
import { rootReducer } from './client/src/store/store';

export default {
  Chat,
  rootReducer,
};
