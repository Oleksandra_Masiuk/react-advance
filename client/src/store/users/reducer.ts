import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { IUser } from '../../interfaces/interfaces';
import {
  getUsers,
  addUser,
  removeUser,
  setUserToEdit,
  editUser,
  setUser,
} from './actions';

interface IInitialState {
  currentUser: IUser | null;
  preloader: boolean;
  editedUser: IUser | null | undefined;
  users: IUser[];
  error: string | undefined | null;
}

const initialState: IInitialState = {
  currentUser: null,
  users: [],
  editedUser: null,
  preloader: false,
  error: null,
};

interface IError {
  message: string;
}

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(getUsers.fulfilled, (state, { payload }) => {
    state.users = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(setUser.fulfilled, (state, { payload }) => {
    state.currentUser = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(setUserToEdit.fulfilled, (state, { payload }) => {
    state.editedUser = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(addUser.fulfilled, (state, { payload }) => {
    state.users = [...state.users, payload];
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(removeUser.fulfilled, (state, { payload }) => {
    state.users = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(editUser.fulfilled, (state, { payload }) => {
    state.users = payload;
    state.editedUser = null;
    state.preloader = false;
    state.error = null;
  });
  builder.addMatcher(
    isAnyOf(
      getUsers.pending,
      setUser.pending,
      setUserToEdit.pending,
      addUser.pending,
      removeUser.pending,
      editUser.pending
    ),
    (state, action) => {
      state.preloader = true;
      state.error = null;
      state.editedUser = null;
    }
  );
  builder.addMatcher(
    isAnyOf(
      getUsers.rejected,
      setUser.rejected,
      setUserToEdit.rejected,
      addUser.rejected,
      editUser.rejected,
      removeUser.rejected
    ),
    (state: IInitialState, { payload }: any) => {
      const pay: IError = payload;
      state.error = pay.message as string;
      state.preloader = false;
      state.editedUser = null;
    }
  );
});
export { reducer };
