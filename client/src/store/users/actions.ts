import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  SET_CURRENT_USER,
  SET_USERS,
  SET_UPDATING_USER,
  ADD_USER,
  UPDATE_USER,
  DELETE_USER,
} from './actionTypes';

import {
  fetchUsers,
  fetchUser,
  deleteUser,
  registerUser,
  updateUser,
  loginUser,
} from '../../services/apiService';
import { ICredentials, IUser } from '../../interfaces/interfaces';

export const getUsers = createAsyncThunk(
  SET_USERS,
  async (_, { rejectWithValue }) => {
    try {
      const users = await fetchUsers();
      return users;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const setUser = createAsyncThunk(
  SET_CURRENT_USER,
  async (payload: ICredentials, { rejectWithValue }) => {
    try {
      const user = await loginUser(payload);
      return user;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const removeUser = createAsyncThunk(
  DELETE_USER,
  async (id: string, { rejectWithValue }) => {
    try {
      const users = await deleteUser(id);
      return users;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const addUser = createAsyncThunk(
  ADD_USER,
  async (payload: IUser, { rejectWithValue }) => {
    try {
      const contact = await registerUser(payload);
      return contact;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const editUser = createAsyncThunk(
  UPDATE_USER,
  async (payload: IUser, { rejectWithValue }) => {
    try {
      const users = await updateUser(payload);
      return users;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const setUserToEdit = createAsyncThunk(
  SET_UPDATING_USER,
  async (id: string, { rejectWithValue }) => {
    try {
      if (!id) return null;
      const message = await fetchUser(id);
      return message;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
