import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  SET_MESSAGES,
  SET_UPDATING_MESSAGE,
  ADD_MESSAGE,
  UPDATE_MESSAGE,
  DELETE_MESSAGE,
} from './actionTypes';

import {
  fetchMessages,
  fetchMessage,
  deleteMessage,
  insertMessage,
  updateMessage,
} from '../../services/apiService';
import { IMessage } from '../../interfaces/interfaces';

export const getMessages = createAsyncThunk(
  SET_MESSAGES,
  async (_, { rejectWithValue }) => {
    try {
      const messages = await fetchMessages();
      return messages;
    } catch (error) {
      return rejectWithValue(error as string);
    }
  }
);

export const getMessage = createAsyncThunk(
  SET_UPDATING_MESSAGE,
  async (id: string, { rejectWithValue }) => {
    try {
      const message = await fetchMessage(id);
      return message;
    } catch (error) {
      return rejectWithValue(error as string);
    }
  }
);

export const removeMessage = createAsyncThunk(
  DELETE_MESSAGE,
  async (id: string, { rejectWithValue }) => {
    try {
      const messages = await deleteMessage(id);
      return messages;
    } catch (error) {
      return rejectWithValue(error as string);
    }
  }
);

export const addMessage = createAsyncThunk(
  ADD_MESSAGE,
  async (payload: IMessage, { rejectWithValue }) => {
    try {
      const contact = await insertMessage(payload);
      return contact;
    } catch (error) {
      return rejectWithValue(error as string);
    }
  }
);

export const editMessage = createAsyncThunk(
  UPDATE_MESSAGE,
  async (payload: IMessage, { rejectWithValue }) => {
    try {
      const message = await updateMessage(payload);
      return message;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const setMessageToEdit = createAsyncThunk(
  SET_UPDATING_MESSAGE,
  async (id: string | null | undefined, { rejectWithValue }) => {
    try {
      if (!id) return null;
      const message = await fetchMessage(id);
      return message;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
