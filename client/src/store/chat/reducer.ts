import { createReducer, isAnyOf } from '@reduxjs/toolkit';
import { IMessage } from '../../interfaces/interfaces';
import {
  getMessages,
  addMessage,
  removeMessage,
  setMessageToEdit,
  editMessage,
} from './actions';

interface IInitialState {
  chat: { messages: IMessage[] };
  preloader: boolean;
  editedMessage: IMessage | null | undefined;
  editModal: boolean;
  error: string | undefined | null;
}

const initialState: IInitialState = {
  chat: { messages: [] },
  preloader: false,
  editedMessage: null,
  editModal: false,
  error: null,
};

interface IError {
  message: string;
}

const reducer = createReducer(initialState, (builder) => {
  builder.addCase(getMessages.fulfilled, (state, { payload }) => {
    payload.sort(
      (a, b) =>
        new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    );
    state.chat.messages = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(addMessage.fulfilled, (state, { payload }) => {
    state.chat.messages = [...state.chat.messages, payload];
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(removeMessage.fulfilled, (state, { payload }) => {
    state.chat.messages = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(setMessageToEdit.fulfilled, (state, { payload }) => {
    state.editModal = !!payload;
    state.editedMessage = payload;
    state.preloader = false;
    state.error = null;
  });
  builder.addCase(editMessage.fulfilled, (state, { payload }) => {
    payload.sort(
      (a, b) =>
        new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
    );
    state.chat.messages = payload;
    state.editModal = false;
    state.editedMessage = null;
    state.preloader = false;
    state.error = null;
  });
  builder.addMatcher(
    isAnyOf(
      getMessages.pending,
      addMessage.pending,
      removeMessage.pending,
      setMessageToEdit.pending,
      editMessage.pending
    ),
    (state, action) => {
      state.preloader = true;
      state.error = null;
    }
  );
  builder.addMatcher(
    isAnyOf(
      getMessages.rejected,
      addMessage.rejected,
      removeMessage.rejected,
      setMessageToEdit.rejected,
      editMessage.rejected
    ),
    (state: IInitialState, { payload }: any) => {
      const pay: IError = payload;
      state.error = pay.message as string;
      state.preloader = false;
      state.editedMessage = null;
    }
  );
});
export { reducer };
