import {
  configureStore,
  combineReducers,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';
import { reducer as chatReducer } from './chat/reducer';
import { reducer as userReducer } from './users/reducer';
import { enableMapSet } from 'immer';

enableMapSet();

export const rootReducer = combineReducers({
  chatReducer,
  userReducer,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: false,
  }),
});

export type RootState = ReturnType<typeof rootReducer>;
export default store;
