import { Routes, Route } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import './App.css';
import Chat from './components/Chat';
import EditUser from './components/EditUser/EditUser';
import Login from './components/Login/Login';
import MainFooter from './components/MainFooter/MainFooter';
import MainHeader from './components/MainHeader/MainHeader';
import UserList from './components/UserList/UserList';
import NotFound from './components/NotFound/NotFound';
import PublicRoute from './components/PublicRoute/PublicRoute';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';

const App = () => {
  return (
    <div className="App">
      <MainHeader />
      <Routes>
        <Route
          path="login"
          element={<PublicRoute Component={Login} restricted />}
        />
        <Route path="users" element={<PrivateRoute Component={UserList} />} />
        <Route
          path="users/edit"
          element={<PrivateRoute Component={EditUser} />}
        />
        <Route
          path="users/edit/:id"
          element={<PrivateRoute Component={EditUser} />}
        />
        <Route path="/" element={<PrivateRoute Component={Chat} />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      <MainFooter />
      <ToastContainer />
    </div>
  );
};

export default App;
