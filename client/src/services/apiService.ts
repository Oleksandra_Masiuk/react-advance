import axios from 'axios';
import { ICredentials, IMessage, IUser } from '../interfaces/interfaces';

axios.defaults.baseURL = 'http://localhost:3001';

export async function fetchMessages(): Promise<IMessage[]> {
  const { data } = await axios.get(`/messages`);
  return data;
}

export async function fetchMessage(id: string): Promise<IMessage> {
  try {
    const { data } = await axios.get(`/messages/${id}`);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}
export async function deleteMessage(id: string): Promise<IMessage[]> {
  try {
    const { data } = await axios.delete(`/messages/${id}`);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}

export async function insertMessage(message: IMessage): Promise<IMessage> {
  const { data } = await axios.post(`/messages`, message);
  return data;
}

export async function updateMessage(message: IMessage): Promise<IMessage[]> {
  try {
    const { data } = await axios.put(`/messages/${message.id}`, message);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}

export async function fetchUsers(): Promise<IUser[]> {
  const { data } = await axios.get(`/users`);
  return data;
}

export async function fetchUser(id: string): Promise<IUser> {
  try {
    const { data } = await axios.get(`/users/${id}`);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}
export async function deleteUser(id: string): Promise<IUser[]> {
  try {
    const { data } = await axios.delete(`/users/${id}`);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}

export async function updateUser(user: IUser): Promise<IUser[]> {
  try {
    const { data } = await axios.put(`/users/${user.id}`, user);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}

export async function registerUser(credentials: IUser): Promise<IUser> {
  try {
    const { data } = await axios.post('users', credentials);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}

export async function loginUser(credentials: ICredentials): Promise<IUser> {
  try {
    const { data } = await axios.post('/users/login', credentials);
    return data;
  } catch (err: any) {
    throw new Error(err.response.data);
  }
}
