import { useSelector } from 'react-redux';
import { FC } from 'react';
import { Navigate } from 'react-router-dom';

import { RootState } from '../../store/store';

interface PrivateRouteProps {
  Component: React.ElementType;
}

const PrivateRoute: FC<PrivateRouteProps> = ({ Component }) => {
  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.userReducer.currentUser,
  }));

  return currentUser ? <Component /> : <Navigate to="/login" />;
};

export default PrivateRoute;
