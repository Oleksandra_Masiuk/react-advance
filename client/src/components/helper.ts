import { IMessage } from '../interfaces/interfaces';
const countUsers = (messages: IMessage[]): number => {
  const users = new Map();
  messages.forEach((message: IMessage) => {
    users.set(
      message.user,
      users.get(message.user) ? 1 + users.get(message.user) : 1
    );
  });
  return users.size;
};
const findLastUserMessage = (messages: IMessage[], currentId: string) => {
  const messageArr = [...messages].reverse();
  const mes = messageArr.find((message) => message.userId === currentId);

  return mes?.id;
};
export { countUsers, findLastUserMessage };
