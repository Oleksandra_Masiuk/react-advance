import { FC, useEffect, useState } from 'react';

import { CHAT_NAME } from '../../constants/chat';
import { setDateFormat } from './helper';
import './Header.css';
import { IMessage } from '../../interfaces/interfaces';
import { countUsers } from '../helper';

interface HeaderProps {
  messages: IMessage[];
}

const Header: FC<HeaderProps> = ({ messages }) => {
  const [userCounter, setUserCounter] = useState(0);
  const [messageCounter, setMessageCounter] = useState(0);
  const [lastMessageDate, setLastMessageDate] = useState('');
  useEffect(() => {
    if (!messages || messages?.length === 0) return;
    const lastDate = messages[messages.length - 1].createdAt;
    setMessageCounter(messages.length);
    setUserCounter(countUsers(messages));
    setLastMessageDate(lastDate ? lastDate : '');
  }, [messages]);

  return (
    <div className="header">
      <div className="header-title">{CHAT_NAME}</div>
      <div className="header-users-count-wrap">
        <div className="header-users-count">{userCounter}</div>
        <span>participants</span>
      </div>
      <div className="header-messages-count-wrap">
        <div className="header-messages-count">{messageCounter}</div>
        <span>messages</span>
      </div>
      <div className="header-last-message-date-wrap">
        <span>last message</span>
        <div className="header-last-message-date">
          {setDateFormat(lastMessageDate)}
        </div>
      </div>
    </div>
  );
};

export default Header;
