const setDateFormat = (oldDate: string): string => {
  if (!oldDate) return '';
  const date = new Date(oldDate);

  let month = ('0' + (date.getMonth() + 1)).slice(-2);
  let hours = ('0' + date.getHours()).slice(-2);
  let minutes = ('0' + date.getMinutes()).slice(-2);
  let day = ('0' + date.getDate()).slice(-2);

  const dateFormat = [day, month, date.getFullYear()].join('.');
  const timeFormat = [hours, minutes].join(':');

  return `${dateFormat} ${timeFormat}`;
};
export { setDateFormat };
