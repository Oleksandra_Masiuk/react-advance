import { FC } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';

import { RootState } from '../../store/store';
import './MainHeader.css';

const MainHeader: FC = () => {
  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.userReducer.currentUser,
  }));

  return (
    <header className="main-header">
      <span className="message-like"></span>
      <span className="header-title">Chats</span>
      {(currentUser?.role === 1 || currentUser?.login === 'admin') && (
        <>
          <NavLink className="navigation-link" to="/">
            Chat
          </NavLink>
          <NavLink className="navigation-link" to="/users">
            Users
          </NavLink>
        </>
      )}
      {currentUser && <span className="user-name">{currentUser.name}</span>}
    </header>
  );
};
export default MainHeader;
