import { setMessageDateFormat } from '../Message/helper';
import { useState, FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setMessageToEdit, editMessage } from '../../store/chat/actions';

import DeleteModal from '../Modal/Modal';
import { IMessage } from '../../interfaces/interfaces';
import EditModal from '../EditModal/EditModal';
import { RootState } from '../../store/store';
import './OwnMessage.css';

interface OwnMessageProps {
  message: IMessage;
  deleteMessage: (id: string) => void;
}

const OwnMessage: FC<OwnMessageProps> = ({ message, deleteMessage }) => {
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();

  const { editedMessage } = useSelector((state: RootState) => ({
    editedMessage: state.chatReducer.editedMessage,
  }));

  const onDelete = () => {
    deleteMessage(message.id);
    setShowModal(false);
  };

  const onEdit = () => {
    dispatch(setMessageToEdit(message.id));
  };

  const onClose = () => {
    dispatch(setMessageToEdit(null));
  };

  const updateMessage = (text: string) => {
    dispatch(
      editMessage({
        ...editedMessage,
        text,
        editedAt: new Date().toString(),
      })
    );
  };

  return (
    <>
      <div className="own-message">
        <div className="message-time">
          <span>{setMessageDateFormat(message.createdAt)}</span>
          {message.editedAt && (
            <span className="message-time-edited">
              {'edited at ' + setMessageDateFormat(message.editedAt)}
            </span>
          )}
        </div>
        <div className="message-text">{message.text}</div>
        <div className="buttons">
          <button className="message-edit" onClick={onEdit}>
            Edit
          </button>
          <button className="message-delete" onClick={() => setShowModal(true)}>
            Delete
          </button>
        </div>
      </div>
      {showModal && (
        <DeleteModal
          onClose={() => setShowModal(false)}
          onDelete={() => onDelete()}
        />
      )}
      <EditModal onClose={onClose} editMessage={updateMessage} />
    </>
  );
};

export default OwnMessage;
