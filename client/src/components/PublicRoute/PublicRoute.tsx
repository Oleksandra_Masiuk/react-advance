import { FC } from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { RootState } from '../../store/store';

interface PublicRouteProps {
  Component: React.ElementType;
  restricted?: boolean;
}

const PublicRoute: FC<PublicRouteProps> = ({
  Component,
  restricted = false,
}) => {
  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.userReducer.currentUser,
  }));

  const shouldRedirect = currentUser && restricted;

  return shouldRedirect ? (
    <Navigate to={currentUser.role ? '/users' : '/'} />
  ) : (
    <Component />
  );
};

export default PublicRoute;
