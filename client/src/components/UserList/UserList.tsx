import { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { IUser } from '../../interfaces/interfaces';
import { getUsers } from '../../store/users/actions';
import Preloader from '../Preloader/Preloader';
import { removeUser } from '../../store/users/actions';
import { RootState } from '../../store/store';
import './UserList.css';

const UserList: FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(getUsers());
  }, [dispatch]);

  const { users, error, preloader } = useSelector((state: RootState) => ({
    users: state.userReducer.users,
    error: state.userReducer.error,
    preloader: state.userReducer.preloader,
  }));

  useEffect(() => {
    if (!error) return;
    toast(error);
  }, [error]);

  return (
    <>
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <button
            onClick={() => {
              navigate('/users/edit');
            }}
            className="user-list-button"
          >
            Add user
          </button>
          <div className="users">
            {users?.length !== 0 &&
              users.map((user: IUser) => {
                if (user.role === 1) return null;
                return (
                  <div className="user-item" key={user.id}>
                    <div className="user-data">
                      <span>{user.name}</span>
                      <span>{user.login}</span>
                    </div>
                    <div className="user-list-buttons">
                      <button
                        onClick={() => {
                          navigate(`/users/edit/${user.id}`);
                        }}
                        className="modal-button-delete"
                      >
                        Edit
                      </button>
                      <button
                        onClick={() => {
                          dispatch(removeUser(user.id));
                        }}
                        className="modal-button-cancel"
                      >
                        Delete
                      </button>
                    </div>
                  </div>
                );
              })}
          </div>
        </>
      )}
    </>
  );
};
export default UserList;
