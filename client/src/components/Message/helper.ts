const setMessageDateFormat = (oldDate: string) => {
  if (!oldDate) return '';
  const date = new Date(oldDate);

  let hours = ('0' + date.getHours()).slice(-2);
  let minutes = ('0' + date.getMinutes()).slice(-2);

  const timeFormat = [hours, minutes].join(':');

  return timeFormat;
};
export { setMessageDateFormat };
