import { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import './Message.css';
import { setMessageDateFormat } from './helper';
import { IMessage } from '../../interfaces/interfaces';
import { editMessage } from '../../store/chat/actions';
import { RootState } from '../../store/store';

interface MessageProps {
  message: IMessage;
}

const Message: FC<MessageProps> = ({ message }) => {
  const dispatch = useDispatch();

  const { currentUserId } = useSelector((state: RootState) => ({
    currentUserId: state.userReducer.currentUser.id,
  }));

  const toggleLike = () => {
    const liked = message.isLiked.join(' ').includes(currentUserId);
    let likes: Array<string> = message.isLiked;
    likes = liked
      ? likes.filter((like) => like !== currentUserId)
      : [...likes, currentUserId];
    dispatch(editMessage({ ...message, isLiked: likes }));
  };

  return (
    <div className="message">
      <div className="message-header">
        <img
          className="message-user-avatar"
          src={message.avatar}
          alt="avatar"
        />
        <div className="message-user-name">{message.user}</div>
        <div className="message-time">
          <span>{setMessageDateFormat(message.createdAt)}</span>
          {message.editedAt && (
            <span className="message-time-edited">
              {'edited at ' + setMessageDateFormat(message.editedAt)}
            </span>
          )}
        </div>
        <button
          className={
            message.isLiked.join(' ').includes(currentUserId)
              ? 'message-liked'
              : 'message-like'
          }
          onClick={toggleLike}
        ></button>
      </div>
      <div className="message-text">{message.text}</div>
    </div>
  );
};

export default Message;
