const getDate = (oldDate: string): string => {
  const from = new Date();
  const date = new Date(oldDate);

  const difference = Math.floor(
    (from.getTime() - date.getTime()) / (1000 * 3600 * 24)
  );

  if (difference === 0) {
    return 'Today';
  }
  if (difference === 1) {
    return 'Yesterday';
  }

  let dayNumber = ('0' + date.getDate()).slice(-2);

  const month = date.toLocaleString('en-US', { month: 'long' });
  const day = date.toLocaleString('en-US', { weekday: 'long' }) + ',';

  return [day, dayNumber, month].join(' ');
};

const groupBy = (array: [any], key: string): object => {
  return array.reduce((result, currentItem) => {
    const d = new Date(currentItem[key]);
    const date = `${d.getDay()}${d.getMonth() + 1}${d.getFullYear()}`;
    (result[date] = result[date] || []).push(currentItem);
    return result;
  }, {});
};

export { getDate, groupBy };
