import { useRef, useEffect, FC, useState } from 'react';
import { useSelector } from 'react-redux';

import Message from '../Message/Message';
import { getDate, groupBy } from './helper';
import { IMessage } from '../../interfaces/interfaces';
import OwnMessage from '../OwnMessage/OwnMessage';
import { RootState } from '../../store/store';
import './MessageList.css';

interface MessageListProps {
  messages: IMessage[] | any;
  deleteMessage: (id: string) => void;
}

const MessageList: FC<MessageListProps> = ({ messages, deleteMessage }) => {
  const [groupedMessages, setGroupedMessages] = useState({});
  const list = useRef<HTMLDivElement>(null);

  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.userReducer.currentUser,
  }));

  useEffect(() => {
    setGroupedMessages(groupBy(messages, 'createdAt'));
    list.current?.lastElementChild?.lastElementChild?.scrollIntoView({
      behavior: 'smooth',
    });
  }, [messages]);

  return (
    <div ref={list} className="message-list">
      {Object.keys(groupedMessages).map((key) => (
        <div key={groupedMessages[key][0].id + groupedMessages[key][0].userId}>
          <div className="messages-divider">
            <div className="messages-divider-date">
              {getDate(groupedMessages[key][0].createdAt)}
            </div>
          </div>
          {groupedMessages[key].map((message: IMessage) =>
            message.userId === currentUser.id ? (
              <OwnMessage
                key={message.id}
                message={message}
                deleteMessage={deleteMessage}
              ></OwnMessage>
            ) : (
              <Message message={message} key={message.id} />
            )
          )}
        </div>
      ))}
    </div>
  );
};
export default MessageList;
