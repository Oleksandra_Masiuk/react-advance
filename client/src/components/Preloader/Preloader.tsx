import { FC } from 'react';
import './Preloader.css';

const Preloader: FC = () => {
  return <div className="preloader"></div>;
};
export default Preloader;
