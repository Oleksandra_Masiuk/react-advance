import { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { v4 as uuid } from 'uuid';
import { toast } from 'react-toastify';

import { setUser } from '../../store/users/actions';
import Preloader from '../Preloader/Preloader';
import { RootState } from '../../store/store';
import './Login.css';

const Login: FC = () => {
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loginId = uuid();
  const passwordId = uuid();
  const dispatch = useDispatch();

  const { preloader, error } = useSelector((state: RootState) => ({
    preloader: state.userReducer.preloader,
    error: state.userReducer.error,
  }));

  useEffect(() => {
    if (!error) return;
    toast(error);
  }, [error]);
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;
    switch (name) {
      case 'login':
        setLogin(value);
        break;
      case 'password':
        setPassword(value);
        break;
      default:
        throw new Error(`Unknown input ${name}`);
    }
  };
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (login.length < 3) {
      return toast('Login should have at least 3 symbols');
    }
    if (password.length < 3) {
      return toast('Password should have at least 3 symbols');
    }

    let re =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (login !== 'admin' && !re.test(login)) {
      return toast(`Please enter valid email in login field`);
    }

    dispatch(setUser({ login, password }));
    reset();
  };

  const reset = () => {
    setLogin('');
    setPassword('');
  };

  return (
    <>
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <h1 className="login-header">Login page</h1>
          <form className="login-form" onSubmit={handleSubmit}>
            <label className="form-label" htmlFor={loginId}>
              Login
              <input
                className="login-input"
                type="name"
                name="login"
                title="Please enter valid login"
                required
                value={login}
                onChange={handleInputChange}
                id={loginId}
              />
            </label>
            <label className="form-label" htmlFor={passwordId}>
              Password
              <input
                className="login-input"
                type="password"
                name="password"
                required
                value={password}
                onChange={handleInputChange}
                id={passwordId}
              />
            </label>
            <button className="login-button" type="submit">
              Login
            </button>
          </form>
        </>
      )}
    </>
  );
};
export default Login;
