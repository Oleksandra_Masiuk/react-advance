import { FC } from 'react';
import './NotFound.css';

const NotFound: FC = () => {
  return (
    <div className="not-found">
      <span>Not found page 404</span>
    </div>
  );
};
export default NotFound;
