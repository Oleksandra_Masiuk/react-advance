import React, { FC, useState } from 'react';
import { useSelector } from 'react-redux';

import './EditModal.css';
import { RootState } from '../../store/store';

interface EditModalProps {
  onClose: () => void;
  editMessage: (text: string) => void;
}

const EditModal: FC<EditModalProps> = ({ onClose, editMessage }) => {
  const { editedMessage, editModal } = useSelector((state: RootState) => ({
    editedMessage: state.chatReducer.editedMessage,
    editModal: state.chatReducer.editModal,
  }));
  const [text, setText] = useState(editedMessage?.text || '');

  const updateMessage = (): void => {
    const trimmedText = text.trim();
    if (trimmedText === '' || editedMessage?.text === trimmedText) return;
    editMessage(text);
  };

  return (
    <div className={`backdrop ${editModal ? '' : 'modal-shown'}`}>
      <div className={`edit-message-modal ${editModal ? 'modal-shown' : ''}`}>
        <input
          className="edit-message-input"
          type="text"
          value={text}
          onChange={(e) => {
            setText(e.target.value);
          }}
        />
        <div className="edit-buttons">
          <button className="edit-message-button" onClick={updateMessage}>
            OK
          </button>
          <button className="edit-message-close" onClick={onClose}>
            Cancel
          </button>
        </div>
      </div>
    </div>
  );
};

export default EditModal;
