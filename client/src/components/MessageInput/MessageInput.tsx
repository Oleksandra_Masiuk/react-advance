import { useState, FC } from 'react';
import { useSelector } from 'react-redux';

import { IMessage } from '../../interfaces/interfaces';
import { RootState } from '../../store/store';
import './MessageInput.css';

interface DeleteModalProps {
  addNewMessage: (message: IMessage) => void;
}

const MessageInput: FC<DeleteModalProps> = ({ addNewMessage }) => {
  const [text, setText] = useState('');

  const { currentUser } = useSelector((state: RootState) => ({
    currentUser: state.userReducer.currentUser,
  }));

  const addMessage = () => {
    if (text.trim() === '') return;
    const message = {
      userId: currentUser.id,
      avatar: currentUser.avatar,
      user: currentUser.name,
      text: text,
      createdAt: new Date().toString(),
      editedAt: '',
      isLiked: [],
      id: '',
    };
    addNewMessage(message);
    setText('');
  };

  return (
    <div className="message-input">
      <input
        value={text}
        className="message-input-text"
        placeholder="Enter your message"
        onChange={(e) => {
          setText(e.target.value);
        }}
      />
      <div className="button-wrapper">
        <button
          className="message-input-button"
          onClick={() => {
            addMessage();
          }}
        >
          Send
        </button>
      </div>
    </div>
  );
};

export default MessageInput;
