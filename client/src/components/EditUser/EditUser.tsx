import { FC, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { v4 as uuid } from 'uuid';
import { toast } from 'react-toastify';

import { setUserToEdit, addUser, editUser } from '../../store/users/actions';
import Preloader from '../Preloader/Preloader';
import { AVATAR } from '../../constants/User';
import { NOT_FOUND_USER } from '../../constants/errors';

const EditUser: FC = () => {
  const { id } = useParams();
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const loginId = uuid();
  const passwordId = uuid();
  const nameId = uuid();

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { preloader, error, editedUser } = useSelector((state: any) => ({
    preloader: state.userReducer.preloader,
    error: state.userReducer.error,
    editedUser: state.userReducer.editedUser,
  }));

  useEffect(() => {
    if (!id) return;
    dispatch(setUserToEdit(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (!editedUser) return;
    setLogin(editedUser.login);
    setPassword(editedUser.password);
    setName(editedUser.name);
  }, [dispatch, editedUser]);

  useEffect(() => {
    if (!error) return;
    toast(error);
    if (error === NOT_FOUND_USER) navigate('/users');
  }, [error, navigate]);

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value, name } = event.target;
    switch (name) {
      case 'login':
        setLogin(value);
        break;
      case 'password':
        setPassword(value);
        break;
      case 'name':
        setName(value);
        break;
      default:
        throw new Error(`Unknown input ${name}`);
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const trimmedName = name.trim();
    const trimmedLogin = login.trim();
    const trimmedPass = password.trim();

    if (trimmedLogin.length < 3)
      return toast('Login should have at least 3 symbols');
    if (trimmedPass.length < 3)
      return toast('Password should have at least 3 symbols');
    if (trimmedName.length < 3)
      return toast('Name should have at least 3 symbols');

    let re =
      // eslint-disable-next-line no-useless-escape
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (login !== 'admin' && !re.test(login)) {
      return toast(`Please enter valid email in login field`);
    }

    if (trimmedPass === 'admin' && trimmedPass === 'admin')
      return toast('You cannot use admin for password and login');
    if (editedUser) {
      if (
        trimmedLogin === editedUser.login &&
        trimmedPass === editedUser.password &&
        trimmedName === editedUser.name
      )
        return toast('Nothing changed');
      dispatch(
        editUser({
          ...editedUser,
          login: trimmedLogin,
          name: trimmedName,
          password: trimmedPass,
        })
      );
    } else {
      dispatch(
        addUser({
          id: '',
          login: trimmedLogin,
          name: trimmedName,
          password: trimmedPass,
          role: 0,
          avatar: AVATAR,
        })
      );
    }
    if (!error) navigate('/users');
  };

  return (
    <>
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <form className="login-form" onSubmit={handleSubmit}>
            <label className="form-label" htmlFor={loginId}>
              Name
              <input
                className="login-input"
                type="name"
                name="name"
                title="Please enter valid name"
                required
                value={name}
                onChange={handleInputChange}
                id={nameId}
              />
            </label>
            <label className="form-label" htmlFor={loginId}>
              Login
              <input
                className="login-input"
                type="name"
                name="login"
                title="Please enter valid login"
                required
                value={login}
                onChange={handleInputChange}
                id={loginId}
              />
            </label>
            <label className="form-label" htmlFor={passwordId}>
              Password
              <input
                className="login-input"
                type="password"
                name="password"
                required
                value={password}
                onChange={handleInputChange}
                id={passwordId}
              />
            </label>
            {editedUser ? (
              <button className="login-button" type="submit">
                Edit user
              </button>
            ) : (
              <button className="login-button" type="submit">
                Add user
              </button>
            )}
          </form>
        </>
      )}
    </>
  );
};
export default EditUser;
