import { useEffect, FC, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';

import Preloader from './Preloader/Preloader';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import Header from './Header/Header';
import { IMessage } from '../interfaces/interfaces';
import {
  getMessages,
  addMessage,
  removeMessage,
  setMessageToEdit,
} from '../store/chat/actions';
import { findLastUserMessage } from './helper';
import { RootState } from '../store/store';

const Chat: FC = () => {
  const dispatch = useDispatch();
  const { messages, preloader, currentUser, error } = useSelector(
    (state: RootState) => ({
      messages: state.chatReducer.chat.messages,
      preloader: state.chatReducer.preloader,
      currentUser: state.userReducer.currentUser,
      error: state.chatReducer.error,
    })
  );

  useEffect(() => {
    dispatch(getMessages());
  }, [dispatch]);

  useEffect(() => {
    if (!error) return;
    toast(error);
  }, [error]);

  const addNewMessage = (message: IMessage): void => {
    dispatch(addMessage(message));
  };

  const deleteMessage = (id: string): void => {
    dispatch(removeMessage(id));
  };

  const onClick = useCallback(
    (event: KeyboardEvent) => {
      if (event.code === 'ArrowUp') {
        const messageId = findLastUserMessage(messages, currentUser.id);
        if (messageId) {
          dispatch(setMessageToEdit(messageId));
        }
      }
    },
    [messages, dispatch, currentUser.id]
  );

  useEffect(() => {
    window.addEventListener('keydown', onClick);

    return () => {
      window.removeEventListener('keydown', onClick);
    };
  }, [onClick]);

  return (
    <div className="chat">
      {preloader ? (
        <Preloader />
      ) : (
        <>
          <Header messages={messages} />
          <MessageList messages={messages} deleteMessage={deleteMessage} />
          <MessageInput addNewMessage={addNewMessage} />
        </>
      )}
    </div>
  );
};

export default Chat;
